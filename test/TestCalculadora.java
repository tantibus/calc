/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import static org.testng.Assert.*;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 *
 * @author ikarus
 */
public class TestCalculadora {
    
    public TestCalculadora() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    @Test
    public void testSuma()
    {
        double resultActual = pruebas_unitarias.Pruebas_Unitarias.suma(10, 5);
        double resultEsperado = 15;
        assertEquals(resultActual,resultEsperado,"Error Sumando");
    }
    @Test
    public void testSuma2()
    {
        double resultActual = pruebas_unitarias.Pruebas_Unitarias.suma(-15, 20);
        double resultEsperado = 5;
        assertEquals(resultActual,resultEsperado,"Error Sumando");
    }
    
    @Test
    public void testResta()
    {
        double resultActual = pruebas_unitarias.Pruebas_Unitarias.resta(10, -5);
        double resultEsperado = 15;
        assertEquals(resultActual,resultEsperado,"Error Restando");
    }
    
    @Test
    public void testResta2()
    {
        double resultActual = pruebas_unitarias.Pruebas_Unitarias.resta(15, 20);
        double resultEsperado = (-5);
        assertEquals(resultActual,resultEsperado,"Error Restando");
    }
    
    @Test
    public void testMultiplicacion()
    {
        double resultActual = pruebas_unitarias.Pruebas_Unitarias.multiplicacion(10, 5);
        double resultEsperado = 50;
        assertEquals(resultActual,resultEsperado,"Error Multiplicando");
    }
    
    @Test
    public void testMultiplicacion2()
    {
        double resultActual = pruebas_unitarias.Pruebas_Unitarias.multiplicacion(10, -7);
        double resultEsperado = -70;
        assertEquals(resultActual,resultEsperado,"Error Multiplicando");
    }
    
    @Test
    public void testMultiplicacion3()
    {
        double resultActual = pruebas_unitarias.Pruebas_Unitarias.multiplicacion(20, 5);
        double resultEsperado = 100;
        assertEquals(resultActual,resultEsperado,"Error Multiplicando");
    }
    @Test
    public void testDivision()
    {
        double resultActual = pruebas_unitarias.Pruebas_Unitarias.division(10, 5);
        double resultEsperado = 2;
        assertEquals(resultActual,resultEsperado,"Error Dividiendo");
    }
    
    @Test
    public void testDivision2()
    {
        double resultActual = pruebas_unitarias.Pruebas_Unitarias.division(-15, 3);
        double resultEsperado = -5;
        assertEquals(resultActual,resultEsperado,"Error Dividiendo");
    }
    
    @Test
    public void testDivision3()
    {
        double resultActual = pruebas_unitarias.Pruebas_Unitarias.division(15, 0);
        double resultEsperado = 0;
        assertEquals(resultActual,resultEsperado,"Error Dividiendo");
    }
    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @BeforeMethod
    public void setUpMethod() throws Exception {
    }

    @AfterMethod
    public void tearDownMethod() throws Exception {
    }
}
